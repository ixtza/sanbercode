<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    protected function index(){
        // 	menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    protected function create(){
        // menampilkan form untuk membuat data pemain film baru
        return view('cast.create');
    }

    protected function store(Request $request){
        // menyimpan data baru ke tabel Cast
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio"  => $request["bio"]
        ]);
        return redirect('/cast');
    }

    protected function show($id){
        // menampilkan detail data pemain film dengan id tertentu
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    protected function edit($id){
        // menampilkan form untuk edit pemain film dengan id tertentu
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    protected function update($id, Request $request){
        // menyimpan perubahan data pemain film (update) untuk id tertentu
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }

    protected function destroy($id){
        // menghapus data pemain film dengan id tertentu
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
